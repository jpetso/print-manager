# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Giovanni Sora <g.sora@tiscali.it>, 2013, 2017, 2018, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-31 00:46+0000\n"
"PO-Revision-Date: 2022-06-29 14:38+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: plasmoid/package/contents/ui/PopupDialog.qml:26
msgid "Search…"
msgstr "Cerca…"

#: plasmoid/package/contents/ui/PopupDialog.qml:71
#: plasmoid/package/contents/ui/printmanager.qml:52
msgid "No printers have been configured or discovered"
msgstr "On non trovava alcun imprimitor configurate o discoperite"

#: plasmoid/package/contents/ui/PopupDialog.qml:71
msgid "No matches"
msgstr "Nulle correspondentias"

#: plasmoid/package/contents/ui/PrinterItem.qml:25
msgid "Resume printing"
msgstr "Reinitia imprimer"

#: plasmoid/package/contents/ui/PrinterItem.qml:25
msgid "Pause printing"
msgstr "Pausar Imprimer"

#: plasmoid/package/contents/ui/PrinterItem.qml:37
msgid "Configure printer..."
msgstr "Configura imprimitor..."

#: plasmoid/package/contents/ui/PrinterItem.qml:42
msgid "View print queue"
msgstr "Vider cauda de imprimer"

#: plasmoid/package/contents/ui/printmanager.qml:31
msgid "Printers"
msgstr "Imprimitores"

#: plasmoid/package/contents/ui/printmanager.qml:36
msgid "There is one print job in the queue"
msgid_plural "There are %1 print jobs in the queue"
msgstr[0] "Il ha un carga de imprimer in le cauda"
msgstr[1] "Il ha %1 cargas de imrimer in le cauda"

#: plasmoid/package/contents/ui/printmanager.qml:45
msgctxt "Printing document name with printer name"
msgid "Printing %1 with %2"
msgstr "Imprimer %1 con %2"

#: plasmoid/package/contents/ui/printmanager.qml:47
msgctxt "Printing with printer name"
msgid "Printing with %1"
msgstr "Imprimer  con %1"

#: plasmoid/package/contents/ui/printmanager.qml:50
msgid "Print queue is empty"
msgstr "Cauda de imprimer es vacue"

#: plasmoid/package/contents/ui/printmanager.qml:126
msgid "Show All Jobs"
msgstr "Monstra omne cargas"

#: plasmoid/package/contents/ui/printmanager.qml:132
msgid "Show Only Completed Jobs"
msgstr "Monstra solmente cargas completate"

#: plasmoid/package/contents/ui/printmanager.qml:138
msgid "Show Only Active Jobs"
msgstr "Monstra solmente cargas active"

#: plasmoid/package/contents/ui/printmanager.qml:149
msgid "&Configure Printers..."
msgstr "&Configura imprimitores..."

#~ msgid "Search for a printer..."
#~ msgstr "Cerca un imprimitor..."

#~ msgid "General"
#~ msgstr "General"

#~ msgid "Active jobs only"
#~ msgstr "Solmente cargas active"

#~ msgid "%1, no active jobs"
#~ msgstr "%1, Necun cargas active"

#~ msgid "%1, %2 active job"
#~ msgid_plural "%1, %2 active jobs"
#~ msgstr[0] "%1, %2 carga active"
#~ msgstr[1] "%1,%2 cargas active"

#~ msgid "%1, no jobs"
#~ msgstr "%1, Necun cargas"

#~ msgid "%1, %2 job"
#~ msgid_plural "%1, %2 jobs"
#~ msgstr[0] "%1, %2 carga"
#~ msgstr[1] "%1, %2 cargas"

#~ msgid "One active job"
#~ msgid_plural "%1 active jobs"
#~ msgstr[0] "Un carga active"
#~ msgstr[1] "%1 cargas active"

#~ msgid "One job"
#~ msgid_plural "%1 jobs"
#~ msgstr[0] "Un carga"
#~ msgstr[1] "%1 Cargas"

#~ msgid "Only show jobs from the following printers:"
#~ msgstr "Monstra cargas solmente pro le sequente imprimitores:"

#~ msgid "Cancel"
#~ msgstr "Cancella"

#~ msgid "Hold"
#~ msgstr "In pausa"

#~ msgid "Release"
#~ msgstr "Revision"

#~ msgid "Owner:"
#~ msgstr "Proprietario:"

#~ msgid "Size:"
#~ msgstr "Dimension:"

#~ msgid "Created:"
#~ msgstr "Create:"

#~ msgid ""
#~ "There is currently no available printer matching the selected filters"
#~ msgstr "Il ha necun imprimitor disponibile per le filtros seligite"
